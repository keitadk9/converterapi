const API_KEY = "0cb1d7e40010e11f88b7";
const URL = "https://free.currconv.com/api/v7/currencies?apiKey="+API_KEY


Vue.config.devtools = true;

app = new Vue({
    el: '#app',
    data: {

        currencies: {},
        amount : 0,
        from : 'EUR',
        to : 'USD',
        convertedValue : 'Converted values',
        loading : false
    },
    mounted() {
        this.getCurrencies();
    },

    computed : {

        //convert currencies from object to array
        formatedCurrencies(){
            return Object.values(this.currencies);
        },
        disabledButton(){
            return this.amount == 0  || !this.amount || this.loading;
        }
    },

    methods: {
        getCurrencies() {
            this.currencies = localStorage.getItem('currencies');
            if (this.currencies) {
                this.currencies = JSON.parse(this.currencies);
                return;
            }

            fetch(URL)
            .then(response => {
                 if (response.ok) {
                    response.json()
                    .then(data => {
                        this.currencies = data.results
                         console.log(this.currencies);
                        localStorage.setItem('currencies', JSON.stringify(this.currencies));
                     });
                }
            })
        },
        getConvertURL(){
            return "https://free.currconv.com/api/v7/convert?q="+this.from+"_"+this.to+"&compact=ultra&apiKey="+API_KEY;    
        },
        convertCurrencies() {
            this.loading = true;
            const convertURL = this.getConvertURL();
            fetch(convertURL)
            .then(response => {
                response.json()
                .then(data=> {
                    const coeef =  data[this.from+"_"+this.to];
                    const symbole =   this.currencies[this.to].currencySymbol;
                    this.convertedValue = ( parseFloat(this.amount) * coeef ).toFixed(2) +" "+ (symbole ? symbole : "");
                    this.loading = false ;
                })
            })
        }

    },

    watch : {
        from(){
            this.convertedValue = 0;
        },
        to(){
            this.convertedValue = 0;
        }
    }
});




window.__VUE_DEVTOOLS_GLOBAL_HOOK__.Vue = app.constructor;